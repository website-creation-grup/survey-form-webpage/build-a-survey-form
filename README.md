# Transportation of the Future - Survey Page

This page presents a brief survey to gather opinions on the future of transportation. It includes:

    Title and Introduction: "Transportation of the Future" with a light survey focused on vehicle types for future use.
    Form Section: Collects basic information such as name, email, and age.
    Power Source Dropdown: A multiple-choice question asking users which power source they think will drive future cars.
    Electric Car Question: Asks if users would consider buying an electric car in the future (radio button selection).
    Improvement Preferences: Allows users to check off areas they’d like to see improved in electric cars.
    Comment Section: A space for additional feedback with a submit button.

Take part in shaping the future of transportation by completing the survey!